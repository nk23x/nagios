#! /usr/bin/env bash

PATH=/sbin:/usr/sbin:/bin:/usr/bin

declare -i STATE_OK=0
declare -i STATE_WARNING=1
declare -i STATE_CRITICAL=2
declare -i STATE_UNKNOWN=3

declare -i ERR_CODE=$STATE_OK

SOFTWARE_NAME="WordPress"
SEARCH_DIR="$1"

OLD_IFS="$IFS"
IFS=$'\n'

if [ -z "$SEARCH_DIR" ]; then
  ERR_MSG="No search directory given!"
  ERR_CODE=$STATE_UNKNOWN
else
  UPSTREAM_VER=$(curl -f http://api.wordpress.org/core/version-check/1.5/ 2>/dev/null | head -n4 | tail -n1)

  if [ -z "$UPSTREAM_VER" ]; then
    ERR_MSG="Could not get remote version!"
    ERR_CODE=$STATE_UNKNOWN
  else
    for file in $(find ${SEARCH_DIR} \( -type d -name '*.bu' -prune \) -o \( -type f -name wp-config.php -print \)); do
      LOCAL_DIR=$(dirname "$file")
      if [ -f "${LOCAL_DIR}/wp-includes/version.php" ] && grep -q '^\$wp_version' ${LOCAL_DIR}/wp-includes/version.php; then
        LOCAL_VER=$(grep '^\$wp_version' ${LOCAL_DIR}/wp-includes/version.php | cut -d "'" -f 2)
        if [ "$LOCAL_VER" != "$UPSTREAM_VER" ]; then
          ERR_CODE=$STATE_CRITICAL
          ERR_MSG="${ERR_MSG}${LOCAL_DIR##$SEARCH_DIR} (${LOCAL_VER}), "
        fi
      fi
    done
  fi
fi

IFS="$OLD_IFS"

case $ERR_CODE in
  $STATE_OK)
    echo "OK: all local ${SOFTWARE_NAME} are up to date (${UPSTREAM_VER})"
  ;;
  $STATE_UNKNOWN)
    echo "UNKNOWN: $ERR_MSG"
  ;;
  $STATE_CRITICAL)
    echo "ERROR: Not up to date (${UPSTREAM_VER}): ${ERR_MSG}" | sed 's/, $//'
  ;;
esac

exit $ERR_CODE
